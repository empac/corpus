# EMPAC corpus

The EMPAC corpus is a collection of subtitles in English and Spanish for videos from the [European Parliament's Multimedia Centre](https://multimedia.europarl.europa.eu).

The corpus has been compiled with the EMPAC [toolkit](https://bitbucket.org/empac/toolkit).

## Description of the corpus

### English

year | texts | subtitles | tokens | duration
---|---:|---:|---:|---:
2009 | 355 | 23120 | 247647 | 40:08:21
2010 | 744 | 48533 | 510721 | 57:18:52
2011 | 563 | 34054 | 372787 | 39:53:46
2012 | 592 | 37392 | 417046 | 43:17:27
2013 | 484 | 34704 | 385908 | 38:57:59
2014 | 356 | 21532 | 244466 | 25:04:52
2015 | 361 | 13939 | 156020 | 16:34:16
2016 | 203 | 6996 | 77264 | 8:16:33
2017 | 158 | 3947 | 38731 | 4:22:39

### Spanish

year | texts | subtitles | tokens | duration
---|---:|---:|---:|---:
2009 | 464 | 41536 | 403839 | 50:55:18
2010 | 757 | 50286 | 493231 | 59:19:53
2011 | 566 | 34452 | 348906 | 39:59:36
2012 | 595 | 38420 | 390905 | 43:23:04
2013 | 483 | 35288 | 361153 | 38:55:51
2014 | 346 | 19031 | 200571 | 21:50:04
2015 | 358 | 13918 | 149752 | 16:29:07
2016 | 194 | 6710 | 73089 | 7:51:47
2017 | 159 | 4019 | 42498 | 4:27:30

### Alignments

category| videos
---|---:
Aligned | 3778
Only English | 38
Only Spanish | 145

## Contents

```
├── README.md
├── LICENSE.md
├── SRT
├── XML
├── NORM
├── VRT
├── SENTS
└── en_es_alignment.txt
```

### SRT

The subtitles files as published at the European Parliament's Multimedia Centre.

An example of the SRT format:

```text
1
00:00:02,480 --> 00:00:04,760
SEPA is the Single Euro Payments Area.

2
00:00:04,920 --> 00:00:08,560
If you find making payments across Europe
risky, expensive and slow.
```

### XML

The subtitles in XML format.

An example of the XML format:

```xml
<?xml version='1.0' encoding='utf-8'?>
<text category="Others" date="2011-12-21 14:59:00" day="21" description="Fraud and hidden charges should be a thing of the past when making EU cross-border payments in a new deal on the Single Euro Payment Area. And more of today's news." duration="00:02:55" id="N001_111221_EN_MF" lang="en" month="12" n_liners="0" n_lines="73" one_liners="7" record="N001-111221" srt_url="https://www.europarltv.europa.eu/programme-files/2777/srt/n001-111221-en_mf.srt" subtitles="40" title="The News: Payments without borders" two_liners="33" type="News" version="" video_url="https://www.europarltv.europa.eu//programme/others/the-news-payments-without-borders" year="2011">
  <subtitle begin="00:00:02,480" chars="38" cps="18.97" duration="2.003" end="00:00:04,760" id="7f53d31b32e80f9ace77d1bcdc2b36fa" n_lines="1" no="1" pause="0">
    <line chars="38" id="fcbc0fb2f6eccdd9e07db2d01a177938" no="1">SEPA is the Single Euro Payments Area.</line>
  </subtitle>
  <subtitle begin="00:00:04,920" chars="67" cps="16.77" duration="3.996" end="00:00:08,560" id="4be0f79232c4ff77f258ec00b2b00651" n_lines="2" no="2" pause="0.002">
    <line chars="41" id="4c0662a14ead2fec5459bb5ef6384044" no="1">If you find making payments across Europe</line>
    <line chars="26" id="864339894e6cb9b2855d3408200c52ce" no="2">risky, expensive and slow.</line>
  </subtitle>
  ...
</text>
```

### NORM

The subtitles with text normalized to get better results with TreeTagger.

### VRT

The subtitles annotated with linguistic information with TreeTagger in VRT format for the CWB.

Tagsets:

- [Spanish](http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/spanish-tagset.txt) 
- [English](http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/Penn-Treebank-Tagset.pdf)

An example of the VRT format:

```xml
<?xml version='1.0' encoding='utf-8'?>
<text category="Others" date="2011-12-21 14:59:00" day="21" description="Fraud and hidden charges should be a thing of the past when making EU cross-border payments in a new deal on the Single Euro Payment Area. And more of today's news." duration="00:02:55" id="N001_111221_EN_MF" lang="en" month="12" n_liners="0" n_lines="73" one_liners="7" record="N001-111221" srt_url="https://www.europarltv.europa.eu/programme-files/2777/srt/n001-111221-en_mf.srt" subtitles="40" title="The News: Payments without borders" two_liners="33" type="News" version="" video_url="https://www.europarltv.europa.eu//programme/others/the-news-payments-without-borders" year="2011" tokens="471">
<subtitle begin="00:00:02,480" chars="38" cps="18.97" duration="2.003" end="00:00:04,760" id="7f53d31b32e80f9ace77d1bcdc2b36fa" n_lines="1" no="1" pause="0" tokens="8">
<line chars="38" id="fcbc0fb2f6eccdd9e07db2d01a177938" no="1" tokens="8">
SEPA    NP  SEPA
is  VBZ be
the DT  the
Single  NP  Single
Euro    NP  Euro
Payments    NNS payment
Area    NP  Area
.   SENT    .
</line>
</subtitle>
<subtitle begin="00:00:04,920" chars="67" cps="16.77" duration="3.996" end="00:00:08,560" id="4be0f79232c4ff77f258ec00b2b00651" n_lines="2" no="2" pause="0.002" tokens="13">
<line chars="41" id="4c0662a14ead2fec5459bb5ef6384044" no="1" tokens="7">
If  IN  if
you PP  you
find    VVP find
making  VVG make
payments    NNS payment
across  IN  across
Europe  NP  Europe
</line>
<line chars="26" id="864339894e6cb9b2855d3408200c52ce" no="2" tokens="6">
risky   JJ  risky
,   ,   ,
expensive   JJ  expensive
and CC  and
slow    JJ  slow
.   SENT    .
</line>
</subtitle>
...
</tex>
```

### SENTS

The subtitles split in sentences with NLTK.

An example of a text with sentence boundaries annotated:

```xml
<?xml version='1.0' encoding='utf-8'?>
<text id="N001_111221_EN_MF" sentences="25">
<s id="d5d2007f70ea5399d07d0f80c8a2465e" no="1" tokens="8">
SEPA
is
the
Single
Euro
Payments
Area
.
</s>
<s id="1d384ff73b04c76c02c95c2cb78567a5" no="2" tokens="13">
If
you
find
making
payments
across
Europe
risky
,
expensive
and
slow
.
</s>
...
</text>
```

### en_es_alignment.txt

The alignment English-Spanish in the format expected by the `cwb-align-import` tool.

A sample of the alignment file:

```text
N003_0211_EN:80	N003_0211_ES:81 N003_0211_ES:82
N003_0211_EN:81	N003_0211_ES:83
N003_0211_EN:82	N003_0211_ES:84
N003_0211_EN:83	N003_0211_ES:85
N003_0211_EN:84	N003_0211_ES:86
N003_0211_EN:85	N003_0211_ES:87
N003_0211_EN:86	N003_0211_ES:88
N003_0211_EN:87	N003_0211_ES:89
N003_0211_EN:88	N003_0211_ES:90
N003_0211_EN:89	N003_0211_ES:91
```

### LICENSE

The license of the corpus, ODC-BY.

### README

This document.
